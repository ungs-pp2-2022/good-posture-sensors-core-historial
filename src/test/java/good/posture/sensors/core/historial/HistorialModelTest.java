package good.posture.sensors.core.historial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import good.posture.sensors.core.historial.discovery.ClassDiscovery;
import good.posture.sensors.extensible.historial.PostureHistorialMemory;

public class HistorialModelTest {

	
	@Test
	public void ca1() {
		ClassDiscovery classDiscovery =  Mockito.mock(ClassDiscovery.class);
		Set<Class> setClass = new HashSet<>();
		Mockito.when(classDiscovery.findAllClasses(Mockito.anyString(), Mockito.anyString()))
		.thenReturn(setClass);
		
		HistorialModel model = new HistorialModel("", classDiscovery);
		assertFalse(model.initialize());
		
	}
	
	@Test
	public void ca2() {

		ClassDiscovery classDiscovery =  Mockito.mock(ClassDiscovery.class);
		Set<Class> setClass = new HashSet<>();
		setClass.add(PostureHistorialMemory.class);
		Mockito.when(classDiscovery.findAllClasses(Mockito.anyString(), Mockito.anyString()))
		.thenReturn(setClass);
		
		HistorialModel model = new HistorialModel("/classDiscovery", classDiscovery);
		assertTrue(model.initialize());
		
	}
	
	@Test
	public void ca3() {

		ClassDiscovery classDiscovery =  Mockito.mock(ClassDiscovery.class);
		Set<Class> setClass = new HashSet<>();
		Mockito.when(classDiscovery.findAllClasses(Mockito.anyString(), Mockito.anyString()))
		.thenReturn(setClass);
		
		HistorialModel model = new HistorialModel(null, classDiscovery);
		assertFalse(model.initialize());
		
	}
}
