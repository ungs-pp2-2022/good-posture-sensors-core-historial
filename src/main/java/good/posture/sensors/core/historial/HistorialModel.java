package good.posture.sensors.core.historial;

import java.util.Observable;
import java.util.Observer;
import java.util.Optional;
import java.util.Set;

import good.posture.sensors.core.historial.discovery.ClassDiscovery;
import good.posture.sensors.interfaces.PostureHistorial;
import good.posture.sensors.interfaces.domain.Posture;

@SuppressWarnings("deprecation")
public class HistorialModel implements Observer {

	private static final String GOOD_POSTURE_SENSORS_EXTENSIBLE_HISTORIAL = "good.posture.sensors.extensible.historial.";
	
	private String discoveryPath;
	
	private ClassDiscovery discovery;
	
	public HistorialModel(String discoveryPath, ClassDiscovery discovery) {
		this.discoveryPath = discoveryPath;
		this.discovery = discovery;
	}

	private PostureHistorial postureHistorial;
	
	public boolean initialize() {
		try {
			Set<Class> classes = discovery.findAllClasses(discoveryPath, GOOD_POSTURE_SENSORS_EXTENSIBLE_HISTORIAL);
			Optional<Class> classOpt = classes.stream().findFirst();

			if (classOpt.isPresent()) {
				Class classImpl = classOpt.get();
				this.postureHistorial = (PostureHistorial) classImpl.newInstance();
				return true;
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void update(Observable o, Object arg) {
		this.postureHistorial.save(new Posture((Integer) arg));
		this.postureHistorial.findAll().forEach(posture -> {
			System.out.print("{ ID: " + posture.getId() + ", VALUE: " + posture.getValue() + "} ");
		});
		System.out.println("");
	}
}
